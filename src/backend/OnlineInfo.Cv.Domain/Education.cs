﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace OnlineInfo.Cv.Domain
{
    public class Education
    {
        public Education()
        {
        }
        
        public string Title { get; set; }
        
        public string School { get; set; }
        
        public Duration Duration { get; set; }
    }
}
