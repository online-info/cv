﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using FluentValidation.Resources;
using FluentValidation.Validators;

namespace OnlineInfo.Cv.Domain.Validators
{
    public class UrlValidator : PropertyValidator, IRegularExpressionValidator
    {
        private readonly Regex _regex;
        public string Expression { get; }

        public UrlValidator() : base(new LanguageStringSource(nameof(UrlValidator)))
        {
            Expression = @"^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+\.[a-z]+(\/[a-zA-Z0-9#]+\/?)*$";
            _regex = new Regex(Expression, RegexOptions.IgnoreCase);
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;

            if (!_regex.IsMatch((string)context.PropertyValue))
            {
                return false;
            }

            return true;
        }
    }
}
