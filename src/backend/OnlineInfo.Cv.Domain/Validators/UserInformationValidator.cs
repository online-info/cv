﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using OnlineInfo.Cv.Domain;

namespace OnlineInfo.Cv.Domain.Validators
{
    public class UserInformationValidator : AbstractValidator<UserInformation>
    {
        public UserInformationValidator()
        {
            RuleFor(u => u.Name).NotNull().MinimumLength(4).MaximumLength(50);
            RuleFor(u => u.Email).EmailAddress();
            RuleFor(u => u.Phone).SetValidator(new PhoneNumberValidator());
            RuleForEach(u => u.Urls).SetValidator(new UrlValidator());
        }
    }
}
