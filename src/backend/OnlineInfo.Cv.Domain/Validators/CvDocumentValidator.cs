﻿using FluentValidation;

namespace OnlineInfo.Cv.Domain.Validators
{
    public class CvDocumentValidator : AbstractValidator<CvDocument>
    {
        public CvDocumentValidator()
        {
            RuleFor(cvDocument => cvDocument.CvName).MaximumLength(20).SetValidator(new CvNameValidator());
            RuleFor(cvDocument => cvDocument.Summary).MaximumLength(500);
            RuleFor(cvDocument => cvDocument.Info).SetValidator(new UserInformationValidator());
            RuleForEach(cvDocument => cvDocument.Education).SetValidator(new EducationValidator());
            RuleForEach(cvDocument => cvDocument.Projects).SetValidator(new ProjectValidator());
            RuleForEach(cvDocument => cvDocument.Experience).SetValidator(new ExperienceValidator());
            RuleForEach(cvDocument => cvDocument.Skills).SetValidator(new SkillValidator());
            RuleForEach(cvDocument => cvDocument.Certificates).SetValidator(new CertificateValidator());
        }
    }
}
