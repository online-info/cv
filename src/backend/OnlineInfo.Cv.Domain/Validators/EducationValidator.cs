﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineInfo.Cv.Domain.Validators
{
    public class EducationValidator : AbstractValidator<Education>
    {
        public EducationValidator()
        {
            RuleFor(education => education.Title).NotEmpty().WithMessage("{PropertyName} cannot be empty");
            RuleFor(education => education.Title).MaximumLength(100);
            RuleFor(education => education.School).NotEmpty().WithMessage("{PropertyName} cannot be empty");
            RuleFor(education => education.School).MaximumLength(100);
            RuleFor(education => education.Duration).SetValidator(new DurationValidator());
        }
    }
}
