﻿using FluentValidation;

namespace OnlineInfo.Cv.Domain.Validators
{
    public class ProjectValidator : AbstractValidator<Project>
    {
        public ProjectValidator()
        {
            RuleFor(project => project.Title).NotEmpty().WithMessage("{PropertyName} can not be empty");
            RuleFor(project => project.Title).MaximumLength(100);
            RuleFor(project => project.Description).MaximumLength(500);
            RuleFor(project => project.Duration).SetValidator(new DurationValidator());
            RuleFor(project => project.Duration).NotNull().WithMessage("{Property} can not be null");
        }
    }
}
