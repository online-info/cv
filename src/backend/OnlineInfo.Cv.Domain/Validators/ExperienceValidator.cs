﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineInfo.Cv.Domain.Validators
{
    public class ExperienceValidator : AbstractValidator<Experience>
    {
        public ExperienceValidator()
        {
            RuleFor(experience => experience.Title).NotEmpty().MaximumLength(100);
            RuleFor(experience => experience.Employer).NotEmpty().MaximumLength(100);
            RuleFor(experience => experience.Description).MaximumLength(500);
            RuleFor(experience => experience.Duration).NotNull().SetValidator(new DurationValidator());
        }
    }
}
