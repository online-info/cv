﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace OnlineInfo.Cv.Domain.Validators
{
    public class DurationValidator : AbstractValidator<Duration>
    {
        public DurationValidator()
        {
            RuleFor(duration => duration.StartDate).NotNull();
            RuleFor(duration => duration.StartDate)
                .LessThan(duration => duration.EndDate)
                .When(duration => duration.EndDate != null);
        }
    }
}
