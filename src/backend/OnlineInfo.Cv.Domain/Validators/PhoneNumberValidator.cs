﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using FluentValidation.Resources;
using FluentValidation.Validators;

namespace OnlineInfo.Cv.Domain.Validators
{
    public class PhoneNumberValidator : PropertyValidator, IRegularExpressionValidator
    {

        private readonly Regex _regex;
        public string Expression { get; }
        
        public PhoneNumberValidator() : base(new LanguageStringSource(nameof(PhoneNumberValidator)))
        {
            Expression = @"^\+[1-9]{1}[0-9]{5,14}$";
            _regex = new Regex(Expression, RegexOptions.IgnoreCase);
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;

            if (!_regex.IsMatch((string)context.PropertyValue))
            {
                return false;
            }

            return true;
        }

    }
}
