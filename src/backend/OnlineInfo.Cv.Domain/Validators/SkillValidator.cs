﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace OnlineInfo.Cv.Domain.Validators
{
    public class SkillValidator : AbstractValidator<Skill>
    {
        public SkillValidator()
        {
            RuleFor(skill => skill.Title).NotEmpty().WithMessage("{PropertyName} can not be empty");
            RuleFor(skill => skill.Title).MaximumLength(50);
        }
    }
}
