﻿using FluentValidation;
using FluentValidation.Resources;
using FluentValidation.Validators;
using System.Text.RegularExpressions;

namespace OnlineInfo.Cv.Domain.Validators
{
    internal class CvNameValidator : PropertyValidator
    {
        private readonly Regex _regex;
        public string Expression { get; }

        public CvNameValidator() : base(new LanguageStringSource(nameof(CvNameValidator)))
        {
            Expression = @"^[a-zA-Z0-9_-]+$";
            _regex = new Regex(Expression, RegexOptions.IgnoreCase);
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            return context.PropertyValue != null && 
                   _regex.IsMatch((string)context.PropertyValue);
        }
    }
}