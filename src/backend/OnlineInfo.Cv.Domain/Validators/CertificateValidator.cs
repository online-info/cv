﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace OnlineInfo.Cv.Domain.Validators
{
    public class CertificateValidator : AbstractValidator<Certificate>
    {
        public CertificateValidator()
        {
            RuleFor(c => c.Title).NotNull().MinimumLength(4).MaximumLength(50);
            RuleFor(c => c.Url).NotNull().SetValidator(new UrlValidator());
        }
    }
}
