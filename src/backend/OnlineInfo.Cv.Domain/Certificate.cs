﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineInfo.Cv.Domain
{
    public class Certificate
    {
        public string Title { get; set; }
        public string Url { get; set; }
    }
}
