﻿using MongoDB.Bson.Serialization.Attributes;
using OnlineInfo.Cv.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace OnlineInfo.Cv.Domain
{
    public class CvDocument : IBase
    {
        public CvDocument()
        {
        }

        [BsonId]
        public Guid Id { get; set; }
        public string CvName { get; set; }
        public string StylePath { get; set; }

        public string Summary { get; set; }
        public UserInformation Info { get; set; }


        public IEnumerable<Certificate> Certificates { get; set; } = new List<Certificate>();
        public IEnumerable<Education> Education { get; set; } = new List<Education>();
        public IEnumerable<Project> Projects { get; set; } = new List<Project>();
        public IEnumerable<Experience> Experience { get; set; } = new List<Experience>();
        public IEnumerable<Skill> Skills { get; set; } = new List<Skill>();
    }
}
