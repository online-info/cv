﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineInfo.Cv.Domain
{
    public class Project
    {
        public Project()
        {
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public Duration Duration { get; set; }
    }
}
