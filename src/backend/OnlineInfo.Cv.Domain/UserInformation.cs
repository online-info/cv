﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineInfo.Cv.Domain
{
    public class UserInformation
    {
        public UserInformation()
        {
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public IEnumerable<string> Urls { get; set; }
    }
}
