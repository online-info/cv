﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineInfo.Cv.Domain.Interfaces
{
    public interface IWriteRepository<T> 
    {
        Task<bool> AddAsync(T entity);
        Task<bool> RemoveAsync(Guid id);
        Task<bool> UpdateAsync(Guid id, T entity);
    }
}
