﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace OnlineInfo.Cv.Domain.Interfaces
{
    public interface IBase
    {
        [BsonId]
        Guid Id { get; set; }
        string CvName { get; set; }
    }
}
