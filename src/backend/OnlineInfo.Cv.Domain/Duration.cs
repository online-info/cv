﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation;
using System.Linq;
using System.Text;
using OnlineInfo.Cv.Domain.Validators;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace OnlineInfo.Cv.Domain
{
    public class Duration
    {      
        public DateTime StartDate { get; private set; }
        public DateTime? EndDate { get; private set; }

        public Duration(DateTime startDate, DateTime? endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }
    }
}
