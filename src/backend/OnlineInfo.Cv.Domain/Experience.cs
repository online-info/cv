﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineInfo.Cv.Domain
{
    public class Experience
    {
        public Experience()
        {
        }

        public string Title { get; set; }

        public string Employer { get; set; }

        public string Description { get; set; }

        public Duration Duration { get; set; }
    }
}
