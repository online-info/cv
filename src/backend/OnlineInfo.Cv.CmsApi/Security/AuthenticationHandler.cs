﻿using System;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace OnlineInfo.Cv.CmsApi.Security
{
    public class AuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private string _password;

        public AuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IConfiguration config)
            : base(options, logger, encoder, clock)
        {
            _password = config["AUTH_PASSWORD"] ?? "develop";
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
                return AuthenticateResult.Fail("Missing Authorization Header");

            try
            {
                var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                if (authHeader.Scheme != "api") return AuthenticateResult.Fail("Invalid Scheme");
                if (authHeader.Parameter != _password) return AuthenticateResult.Fail("Invalid token");
            }
            catch
            {
                return AuthenticateResult.Fail("Invalid Authorization Header");
            }
            

            var identity = new ClaimsIdentity("api");

            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal,null, Scheme.Name);

            return AuthenticateResult.Success(ticket);
        }
    }
}