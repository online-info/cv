﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineInfo.Cv.Domain;
using OnlineInfo.Cv.Domain.Interfaces;
using System;
using System.Threading.Tasks;

namespace OnlineInfo.Cv.CmsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "api")]
    public class CvController : ControllerBase
    {
        private readonly IReadRepository<CvDocument> _readRepository;
        private readonly IWriteRepository<CvDocument> _writeRepository;

        public CvController(IReadRepository<CvDocument> readRepository,
                            IWriteRepository<CvDocument> writeRepository)
        {
            _readRepository = readRepository;
            _writeRepository = writeRepository;
        }


        /// <summary>
        /// Add or update a CV document
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <response code="200">Resource updated successfully</response>
        /// <response code="201">Returns the newly created resource</response>
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]CvDocument document)
        {
            // Validate input
            if (!ModelState.IsValid) { return BadRequest(ModelState.ErrorCount); }

            if (document.Id != default(Guid))
            {
                // check if Id exists in db
                var cvDocument = _readRepository.GetAsync(document.Id);
                if (cvDocument != null)
                {

                    if (await _writeRepository.UpdateAsync(document.Id, document))
                    {
                        return NoContent();
                        //update document, return 204
                    }
                    else
                    {
                        return BadRequest("Could not update document");
                        // return 500, could not update document
                    }
                }
            }
            // else, create new id and insert into db, return 201, + path
            var result = await _writeRepository.AddAsync(document);
            if (result)
            {
                return Ok(document);
            }
            else
            {
                return BadRequest("CvName already exists");
            }
        }



        /// <summary>
        /// Add or update a CV document
        /// </summary>
        /// <param name="id">Cv id for the document you want to remove</param>
        /// <returns></returns>
        /// <response code="204">Returns the newly created resource</response>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] Guid id)
        {
            // Validate input
            if (!ModelState.IsValid) { return BadRequest(ModelState.ErrorCount); }

            if (id != default(Guid))
            {
                // check if Id exists in db
                var cvDocument = _readRepository.GetAsync(id);
                if (cvDocument != null)
                {

                    if (await _writeRepository.RemoveAsync(id))
                    {
                        return NoContent();
                        //update document, return 204
                    }
                    else
                    {
                        return BadRequest("Could not delete document");
                        // return 500, could not update document
                    }
                }
            }
            // else, create new id and insert into db, return 201, + path
            return BadRequest($"Document not found with id: {id}");

        }

    }
}