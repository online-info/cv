﻿using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OnlineInfo.Cv.CmsApi.Security;
using OnlineInfo.Cv.Domain;
using OnlineInfo.Cv.Domain.Interfaces;
using OnlineInfo.Cv.Domain.Validators;
using OnlineInfo.Cv.Infrastructure;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;


namespace OnlineInfo.Cv.CmsApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<CvDocumentValidator>())
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new Info
                {
                    Title = "CV CMS-API",
                    Version = "v1",
                    Description = "CSM-API that can be used to add, update and delete data",
                    Contact = new Contact
                    {
                        Name = "Erik Hansson",
                        Email = string.Empty
                    },
                    License = new License
                    {
                        Name = "MIT",
                        Url = "https://opensource.org/licenses/MIT"
                    }
                });
                config.AddSecurityDefinition("api", new ApiKeyScheme
                {
                    Description = "apikey Example: \"Authorization: api {apikey}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                config.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "api", new string[] { } }
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                config.IncludeXmlComments(xmlPath);
            });
            services.AddAuthentication(o => o.AddScheme("api", a => a.HandlerType = typeof(AuthenticationHandler)));

            services.AddScoped<MongoContext>();

            services.AddScoped<IWriteRepository<CvDocument>, WriteRepository>();
            services.AddScoped<IReadRepository<CvDocument>, ReadRepository<CvDocument>>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            var proxyPrefix = Environment.GetEnvironmentVariable("PROXY_PREFIX") ?? "";

            app.UseSwagger(config =>
            {
                config.PreSerializeFilters.Add((swaggerDoc, httpReq) => swaggerDoc.BasePath = proxyPrefix);
            });

            app.UseSwaggerUI(config =>
            {
                config.SwaggerEndpoint($"{proxyPrefix}/swagger/v1/swagger.json", "CV CMS-API V1");
                config.RoutePrefix = string.Empty;
            });

            app.UseMvc();
        }
    }
}
