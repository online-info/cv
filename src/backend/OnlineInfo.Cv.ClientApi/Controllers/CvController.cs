﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineInfo.Cv.Domain;
using OnlineInfo.Cv.Domain.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineInfo.Cv.ClientApi.Controllers
{
    [Route("api/[controller]")]
    public class CvController : Controller
    {
        private readonly IReadRepository<CvDocument> _readRepository;

        public CvController(IReadRepository<CvDocument> readRepository)
        {
            _readRepository = readRepository;
        }

        // GET api/<controller>/5
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _readRepository.GetAllAsync());
        }

        // GET api/<controller>/name
        /// <summary>
        /// Get a single CV by its name
        /// </summary>
        /// <param name="cvName">CV identifier</param>
        /// <returns></returns> 
        /// <response code="200">Returns requested resource</response>
        [HttpGet("{cvName}")]
        public async Task<IActionResult> Get(string cvName)
        {
            var documentToReturn = await _readRepository.GetByNameAsync(cvName);

            if (documentToReturn == null)
                return NotFound();

            return Ok(documentToReturn);
        }
    }
}
