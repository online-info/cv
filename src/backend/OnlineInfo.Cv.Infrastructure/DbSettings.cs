﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineInfo.Cv.Infrastructure
{
    public class DbSettings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}
