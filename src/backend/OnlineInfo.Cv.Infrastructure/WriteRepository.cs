﻿using MongoDB.Driver;
using OnlineInfo.Cv.Domain;
using OnlineInfo.Cv.Domain.Interfaces;
using System;
using System.Threading.Tasks;

namespace OnlineInfo.Cv.Infrastructure
{
    public class WriteRepository : IWriteRepository<CvDocument>
    {
        private MongoContext _context;

        public WriteRepository(MongoContext context)
        {
            _context = context;
        }

        public async Task<bool> AddAsync(CvDocument entity)
        {
            var cvNameExists = await _context.MongoDatabase.GetCollection<CvDocument>(typeof(CvDocument).Name).FindAsync(x => x.CvName == entity.CvName);
            if (!cvNameExists.Any())
            {
                await _context.MongoDatabase.GetCollection<CvDocument>(typeof(CvDocument).Name).InsertOneAsync(entity);
                return true;
            }
            return false;
        }

        public async Task<bool> RemoveAsync(Guid id)
        {
            var result = await _context.MongoDatabase.GetCollection<CvDocument>(typeof(CvDocument).Name).DeleteOneAsync(x => x.Id == id);
            return result.DeletedCount > 0;
        }

        public async Task<bool> UpdateAsync(Guid id, CvDocument entity)
        {
            var result = await _context.MongoDatabase.GetCollection<CvDocument>(typeof(CvDocument).Name).ReplaceOneAsync(x => x.Id == id, entity);
            return result.ModifiedCount > 0;
        }
    }
}
