﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using OnlineInfo.Cv.Domain;
using System.Diagnostics;

namespace OnlineInfo.Cv.Infrastructure
{
    public class MongoContext
    {
        private string _dbName;
        private string _dbUser;
        private string _dbPassword;

        public IMongoDatabase MongoDatabase { get; }


        public MongoContext(IOptions<DbSettings> settings, IConfiguration configuration, ILogger<MongoContext> logger)
        {
            _dbName = configuration["MONGO_INITDB_DATABASE"];
            _dbUser = configuration["MONGO_INITDB_ROOT_USERNAME"];
            _dbPassword = configuration["MONGO_INITDB_ROOT_PASSWORD"];

            var connectionString = $"mongodb://{_dbUser}:{_dbPassword}@mongodb/{_dbName}";


            logger.LogInformation("Trying to connect with: " + connectionString);
            MongoClient client = new MongoClient(connectionString);

            if (client != null)
            {
                MongoDatabase = client.GetDatabase(_dbName);
            }
        }
    }
}
