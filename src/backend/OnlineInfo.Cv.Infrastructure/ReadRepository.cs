﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using OnlineInfo.Cv.Domain;
using OnlineInfo.Cv.Domain.Interfaces;

namespace OnlineInfo.Cv.Infrastructure
{
    public class ReadRepository<TEntity> : IReadRepository<TEntity> where TEntity : IBase
    {
        private MongoContext _context;
        private IMongoCollection<TEntity> _collection;

        public ReadRepository(MongoContext context)
        {
            _context = context;
            _collection = _context.MongoDatabase.GetCollection<TEntity>(typeof(TEntity).Name);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            try
            {
                var result = await _collection.Find(_ => true).ToListAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TEntity> GetAsync(Guid id)
        {
            try
            {
                var collection = await _collection.FindAsync(x => x.Id == id);
                var result = await collection.FirstOrDefaultAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TEntity> GetByNameAsync(string cvName)
        {
            try
            {
                var collection = await _collection.FindAsync(x => x.CvName == cvName);

                var result = await collection.FirstOrDefaultAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
