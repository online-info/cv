﻿using OnlineInfo.Cv.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using OnlineInfo.Cv.Domain.Interfaces;

namespace OnlineInfo.Cv.Application
{
    public class TestCommand
    {
        ITestRepo _repo;
        public TestCommand(ITestRepo repo)
        {
            _repo = repo;
        }

        public string DoStuff()
        {
           return _repo.Test();
        }
    }
}
