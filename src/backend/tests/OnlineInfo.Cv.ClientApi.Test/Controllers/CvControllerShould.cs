﻿using FakeItEasy;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using OnlineInfo.Cv.ClientApi.Controllers;
using OnlineInfo.Cv.Domain;
using OnlineInfo.Cv.Domain.Interfaces;
using System;
using System.Threading.Tasks;
using Xunit;

namespace OnlineInfo.Cv.ClientApi.Test.Controllers
{
    public class CvControllerShould
    {
        private readonly CvController _sut;

        public CvControllerShould()
        {
            var fakeReadRepository = A.Fake<IReadRepository<CvDocument>>();
            A.CallTo(() => fakeReadRepository.GetByNameAsync(A<string>.That.Not.IsEqualTo("valid")))
                .Returns(Task.FromResult<CvDocument>(null));
            _sut = new CvController(fakeReadRepository);
        }

        [Fact]
        public async Task ReturnsOk_GivenIdExists()
        {
            var result = await _sut.Get("valid");

            var okResult = result as OkObjectResult;

            okResult.Should().NotBeNull();
            okResult.StatusCode.Should().Be(200);
        }

        [Fact]
        public async Task ReturnNotFound_GivenIdNotExists()
        {
            var result = await _sut.Get("invalid");

            var notFoundResult = result as NotFoundResult;

            notFoundResult.StatusCode.Should().Be(404);
        }
    }
}
