﻿using FakeItEasy;
using OnlineInfo.Cv.CmsApi.Controllers;
using OnlineInfo.Cv.Domain;
using OnlineInfo.Cv.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OnlineInfo.Cv.CmsApi.Test.Controllers
{
    public class CvControllerShould
    {
        private readonly CvController _sut;

        public CvControllerShould()
        {
            IReadRepository<CvDocument> fakeReadRepo = A.Fake<IReadRepository<CvDocument>>();
            IWriteRepository<CvDocument> fakeWriteRepo = A.Fake<IWriteRepository<CvDocument>>();

            _sut = new CmsApi.Controllers.CvController(fakeReadRepo, fakeWriteRepo);
        }

        // TODO: Write relevant tests
    }
}
