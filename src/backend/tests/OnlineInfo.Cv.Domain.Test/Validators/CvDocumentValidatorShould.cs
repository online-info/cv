﻿using FluentAssertions;
using OnlineInfo.Cv.Domain.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace OnlineInfo.Cv.Domain.Test.Validators
{
    public class CvDocumentValidatorShould
    {
        private readonly CvDocumentValidator _sut;

        public CvDocumentValidatorShould()
        {
            _sut = new CvDocumentValidator();
        }

        [Fact]
        public void ReturnInvalidResult_GivenInvalidSummary()
        {
            CvDocument document = new CvDocument() { Summary = new string('a', 501) };

            var validationResult = _sut.Validate(document);

            validationResult.IsValid.Should().BeFalse();
            validationResult.Errors
                .Count(error => error.PropertyName == nameof(document.Summary))
                .Should()
                .BePositive();
        }

        [Fact]
        public void ValidateUserInfoObject()
        {
            CvDocument document = new CvDocument()
            {
                Info = new UserInformation()
                {
                    Name = "",
                    Email = "",
                    Phone = "",
                    Urls = new[] { "" }
                }
            };

            var validationResult = _sut.Validate(document);

            validationResult.IsValid.Should().BeFalse();
        }

        [Fact]
        public void ValidateValidMinimumInputs()
        {
            CvDocument document = new CvDocument()
            {
                CvName = "2222222222sssssssssssssssss2ssssssssssssss",

            };
            var validationResult = _sut.Validate(document);
            validationResult.Errors.Should().Contain(error => error.ErrorMessage.Contains("20"));
        }

        [Theory]
        [MemberData(nameof(InvalidNames))]
        public void ValidateCvNameForInvalidInputs(string name)
        {
            CvDocument document = new CvDocument()
            {
                CvName = name

            };
            var validationResult = _sut.Validate(document);
            validationResult.IsValid.Should().BeFalse();
        }

        public static IEnumerable<object[]> InvalidNames()
        {
            yield return new object[] { "häj" };
            yield return new object[] { "hår" };
            yield return new object[] { "hör" };
            yield return new object[] { "ett namn" };
            yield return new object[] { "ettnamnsomärförlångtsdfdsffsd" };
        }


        [Theory]
        [MemberData(nameof(InvalidNames))]
        public void ValidateCvNameForInputs(string name)
        {
            CvDocument document = new CvDocument()
            {
                CvName = name

            };
            var validationResult = _sut.Validate(document);
            validationResult.IsValid.Should().BeFalse();
        }

        public static IEnumerable<object[]> ValidNames()
        {
            yield return new object[] { "erik" };
            yield return new object[] { "erik-hansson" };
            yield return new object[] { "erik_hansson" };
            yield return new object[] { "erik1-2_3" };
        }


        [Fact]
        public void ValidateAllEducationObjects()
        {
            CvDocument document = new CvDocument()
            {
                Summary = "Valid",
                Education = new List<Education>()
                {
                    new Education { Title = "", Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(1d)), School = ""},
                    new Education { Title = "Valid", Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(-1d)), School = "Valid"}
                }
            };

            var validationResult = _sut.Validate(document);
            validationResult.Errors.Count.Should().BeGreaterThan(0);
        }

        [Fact]
        public void ValidateAllProjectObjects()
        {
            CvDocument document = new CvDocument()
            {
                Summary = "Valid",
                Projects = new List<Project>()
                {
                    new Project { Title = "", Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(1d)), Description="Optional"},
                    new Project { Title = "Valid", Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(-1d)), Description="Optional"}
                }
            };

            var validationResult = _sut.Validate(document);

            validationResult.IsValid.Should().BeFalse();
        }

        [Fact]
        public void ValidateAllExperienceObjects()
        {
            CvDocument document = new CvDocument()
            {
                Summary = "Short",
                Experience = new List<Experience>()
                {
                    new Experience()
                    {
                        Title ="",
                        Employer ="",
                        Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(1d))
                    },
                    new Experience()
                    {
                        Title =new string('a', 101),
                        Employer =new string('a', 101),
                        Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(-1d))
                    }
                }
            };

            var validationResult = _sut.Validate(document);

            validationResult.IsValid.Should().BeFalse();
        }

        [Fact]
        public void ValidateAllSkillObjects()
        {
            CvDocument document = new CvDocument()
            {
                Summary = "Short",
                Skills = new List<Skill>()
                {
                    new Skill() { Title = "" },
                    new Skill() { Title = new string('a', 51) }
                }
            };

            var validationResult = _sut.Validate(document);

            validationResult.IsValid.Should().BeFalse();
        }

        [Fact]
        public void ValidateAllCertificateObjects()
        {
            CvDocument document = new CvDocument()
            {
                Summary = "Short",
                Certificates = new List<Certificate>()
                {
                    new Certificate()
                    {
                        Title = "",
                        Url = null
                    },
                    new Certificate()
                    {
                        Title = new string('a', 51),
                        Url = ""
                    },
                    new Certificate()
                    {
                        Title = null,
                        Url = "ww.test.se"
                    },
                    new Certificate()
                    {
                        Title = "hej",
                        Url = "www.test."
                    }
                }
            };

            var validationResult = _sut.Validate(document);
            validationResult.IsValid.Should().BeFalse();
        }
    }
}
