﻿using FluentAssertions;
using OnlineInfo.Cv.Domain.Validators;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace OnlineInfo.Cv.Domain.Test.Validators
{
    public class EducationValidatorShould
    {
        private readonly EducationValidator _sut;

        public EducationValidatorShould()
        {
            _sut = new EducationValidator();
        }

        [Fact]
        public void ReturnInvalidResult_GivenMissingTitle()
        {
            Education invalidEducation = new Education()
            {
                Title = "",
                Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(1d)),
                School = "School"
            };

            var validationResult = _sut.Validate(invalidEducation);

            validationResult.IsValid.Should().BeFalse();
        }

        [Fact]
        public void ReturnInvalidResult_GivenMissingSchool()
        {
            Education invalidEducation = new Education()
            {
                Title = "My education",
                Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(1d)),
                School = ""
            };

            var validationResult = _sut.Validate(invalidEducation);

            validationResult.IsValid.Should().BeFalse();
        }

        [Fact]
        public void ValidateComplexProperties()
        {
            Duration invalidDuration = new Duration(DateTime.Now, DateTime.Now.AddDays(-1d));

            Education invalidEducation = new Education()
            {
                Title = "My education",
                Duration = invalidDuration,
                School = ""
            };

            var validationResult = _sut.Validate(invalidEducation);

            validationResult.IsValid.Should().BeFalse();
            validationResult.Errors.Count.Should().Be(2);
        }

        [Fact]
        public void ReturnInvalidResult_GivenExceededStringLength()
        {
            // Title and School should only be allowed to be 100 characters long
            Education invalidEducation = new Education()
            {
                Title = "OngLfLZtcYUkEcxdskTjDfsShfpXaEuTnHJpjTYyChkKnTkFepROngLfLZtcYUkEcxdskTjDfsShfpXaEuTnHJpjTYyChkKnTkFepR",
                Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(1d)),
                School = "OngLfLZtcYUkEcxdskTjDfsShfpXaEuTnHJpjTYyChkKnTkFepROngLfLZtcYUkEcxdskTjDfsShfpXaEuTnHJpjTYyChkKnTkFepR"
            };

            var validationResult = _sut.Validate(invalidEducation);

            validationResult.IsValid.Should().BeFalse();
            validationResult.Errors.Count.Should().Be(2);
        }

    }
}
