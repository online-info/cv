﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentAssertions;
using OnlineInfo.Cv.Domain.Validators;
using Xunit;

namespace OnlineInfo.Cv.Domain.Test.Validators
{
    public class CertificateValidatorShould
    {
        private readonly CertificateValidator _sut;

        public CertificateValidatorShould()
        {
            _sut = new CertificateValidator();
        }

        [Theory]
        [MemberData(nameof(GetInvalidUrls))]
        public async void ReturnsInvalidResult_GivenInvalidUrl(string title,
            string url)
        {
            var invalidCertificate = new Certificate
            {
                Title = title,
                Url = url
            };

            var result = await _sut.ValidateAsync(invalidCertificate);

            result.IsValid.Should().BeFalse();
        }

        [Theory]
        [MemberData(nameof(GetInvalidTitles))]
        public async void ReturnsInvalidResult_GivenInvalidTitle(string title,
            string url)
        {
            var invalidCertificate = new Certificate
            {
                Title = title,
                Url = url
            };

            var result = await _sut.ValidateAsync(invalidCertificate);

            result.IsValid.Should().BeFalse();
        }

        public static IEnumerable<object[]> GetInvalidUrls()
        {
            yield return new object[] { "MLND", null };
            yield return new object[] { "MLND", "" };
            yield return new object[] { "MLND", "ww.test.se" };
            yield return new object[] { "MLND", "www.test." };
        }

        public static IEnumerable<object[]> GetInvalidTitles()
        {
            yield return new object[] { null, "www.test.com/hej/123" };
            yield return new object[] { "", "www.test.com/hej/123" };
            yield return new object[] { "hej", "www.test.com/hej/123" };
            yield return new object[] { new string('a', 51), "www.test.com/hej/123" };
        }

    }

}
