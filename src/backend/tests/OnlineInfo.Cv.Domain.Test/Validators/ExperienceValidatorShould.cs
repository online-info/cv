﻿using FluentAssertions;
using OnlineInfo.Cv.Domain.Validators;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace OnlineInfo.Cv.Domain.Test.Validators
{
    public class ExperienceValidatorShould
    {
        private readonly ExperienceValidator _sut;

        public ExperienceValidatorShould()
        {
            _sut = new ExperienceValidator();
        }

        [Theory]
        [MemberData(nameof(GetInvalidStrings))]
        public void ReturnInvalidResult_GivenInvalidStrings(string title, string description, string employer)
        {
            Experience invalidExperience = new Experience()
            {
                Title = title,
                Description = description,
                Employer = employer,
                Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(1d))
            };

            var validationResult = _sut.Validate(invalidExperience);

            validationResult.IsValid.Should().BeFalse();
        }

        [Theory]
        [MemberData(nameof(GetInvalidDurations))]
        public void ValidateComplexProperties(Duration invalidDuration)
        {
            string validString = "Valid";
            Experience invalidExperience = new Experience()
            {
                Title = validString,
                Description = validString,
                Employer = validString,
                Duration = invalidDuration
            };

            var validationResult = _sut.Validate(invalidExperience);

            validationResult.IsValid.Should().BeFalse();
        }

        public static IEnumerable<object[]> GetInvalidStrings()
        {
            yield return new object[] { "", "", "" };
            yield return new object[] { new string('a', 101), new string('a', 501), new string('a', 101) };
        }

        public static IEnumerable<object[]> GetInvalidDurations()
        {
            yield return new object[] { new Duration(DateTime.Now, DateTime.Now.AddDays(-1d))};
            yield return new object[] { null };

        }
    }
}
