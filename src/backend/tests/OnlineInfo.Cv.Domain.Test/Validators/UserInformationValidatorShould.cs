﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentAssertions;
using OnlineInfo.Cv.Domain.Validators;
using Xunit;

namespace OnlineInfo.Cv.Domain.Test.Validators
{
    public class UserInformationValidatorShould
    {
        private readonly UserInformationValidator _sut;

        public UserInformationValidatorShould()
        {
            _sut = new UserInformationValidator();
        }

        [Theory]
        [MemberData(nameof(GetInvalidNames))]
        public async void ReturnInvalidResult_GivenInvalidNames(string name, 
                                                                string email,
                                                                string phone, 
                                                                IEnumerable<string> urls)
        {
            var invalidUserInformation = new UserInformation
            {
                Name = name,
                Email = email,
                Phone = phone,
                Urls = urls
            };

            var result = await _sut.ValidateAsync(invalidUserInformation);

            result.IsValid.Should().BeFalse();
        }

        [Theory]
        [MemberData(nameof(GetInvalidEmails))]
        public async void ReturnInvalidResult_GivenInvalidEmails(string name,
                                                                 string email, 
                                                                 string phone,
                                                                 IEnumerable<string> urls)
        {
            var invalidUserInformation = new UserInformation
            {
                Name = name,
                Email = email,
                Phone = phone,
                Urls = urls
            };

            var result = await _sut.ValidateAsync(invalidUserInformation);

            result.IsValid.Should().BeFalse();
        }

        [Theory]
        [MemberData(nameof(GetInvalidPhoneNumbers))]
        public async void ReturnInvalidResult_GivenInvalidPhoneNumbers(string name, 
                                                                       string email, 
                                                                       string phone,
                                                                       IEnumerable<string> urls)
        {
            var invalidUserInformation = new UserInformation
            {
                Name = name,
                Email = email,
                Phone = phone,
                Urls = urls
            };

            var result = await _sut.ValidateAsync(invalidUserInformation);

            result.IsValid.Should().BeFalse();
        }

        [Theory]
        [MemberData(nameof(GetInvalidUrls))]
        public async void ReturnsInvalidResult_GivenInvalidUrls(string name,
                                                                string email,
                                                                string phone,
                                                                IEnumerable<string> urls)
        {
            var invalidUserInformation = new UserInformation
            {
                Name = name,
                Email = email,
                Phone = phone,
                Urls = urls
            };

            var result = await _sut.ValidateAsync(invalidUserInformation);

            result.IsValid.Should().BeFalse();
        }


        public static IEnumerable<object[]> GetInvalidEmails()
        {
            yield return new object[] { "validname", "", "+46746732542", new List<string> { "www.valid.test" } };
            yield return new object[] { "validname", "@gmail.com", "+46746732542", new List<string> { "www.valid.test" } };
            yield return new object[] { "validname", "namegmail.com", "+46746732542", new List<string> { "www.valid.test" } };
            yield return new object[] { "validname", "name@gmailcom", "+46746732542", new List<string> { "www.valid.test" } };
        }

        public static IEnumerable<object[]> GetInvalidUrls()
        {
            yield return new object[] {"validname", "test@gmail.com", "+46746732542", new List<string> { "" } };
            yield return new object[] { "validname", "test@gmail.com", "+46746732542", new List<string> { "ww.test.se" } };
            yield return new object[] { "validname", "test@gmail.com", "+46746732542", new List<string> { "www.test." } };
        }

        public static IEnumerable<object[]> GetInvalidPhoneNumbers()
        {
            yield return new object[] { "validname", "valid@mail.com", "", new List<string> { "www.valid.test" } };
            yield return new object[] { "validname", "valid@mail.com", "+46092", new List<string> { "www.valid.test" } };
            yield return new object[] { "validname", "valid@mail.com", "46737694365", new List<string> { "www.valid.test" } };
            yield return new object[] { "validname", "valid@mail.com", "+46092876a7", new List<string> { "www.valid.test" } };
        }

        public static IEnumerable<object[]> GetInvalidNames()
        {
            yield return new object[] { null, "valid@mail.com", "+46746732542", new List<string> { "www.valid.test" } };
            yield return new object[] { "", "valid@mail.com", "+46746732542", new List<string> { "www.valid.test" } };
            yield return new object[] { "a", "valid@mail.com", "+46746732542", new List<string> { "www.valid.test" } };
            yield return new object[] { "Mu", "valid@mail.com", "+46746732542", new List<string> { "www.valid.test" } };
            yield return new object[] { "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "valid@mail.com", "+46746732542", new List<string> { "www.valid.test" } };
        }
    }
}
