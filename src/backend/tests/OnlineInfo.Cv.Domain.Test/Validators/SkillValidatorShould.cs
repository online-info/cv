﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentAssertions;
using OnlineInfo.Cv.Domain.Validators;
using Xunit;

namespace OnlineInfo.Cv.Domain.Test.Validators
{
    public class SkillValidatorShould
    {
        private readonly SkillValidator _sut;

        public SkillValidatorShould()
        {
            _sut = new SkillValidator();
        }

        [Fact]
        public void ReturnInvalidResult_GivenMissingTitle()
        {
            Skill invalidSkill = new Skill
            {
                Title = ""
            };

            var validationResult = _sut.Validate(invalidSkill);

            validationResult.IsValid.Should().BeFalse();
        }

        [Fact]
        public void ReturnInvalidResult_GivenExceededStringLength()
        {
            Skill invalidSkill = new Skill
            {
                Title = new string('a', 51)
            };

            var validationResult = _sut.Validate(invalidSkill);

            validationResult.IsValid.Should().BeFalse();
        }
    }
}
