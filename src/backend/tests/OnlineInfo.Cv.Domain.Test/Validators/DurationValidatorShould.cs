﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using OnlineInfo.Cv.Domain.Validators;
using Xunit;

namespace OnlineInfo.Cv.Domain.Test.Validators
{
    public class DurationValidatorShould
    {
        private readonly DurationValidator _sut;

        public DurationValidatorShould()
        {
            _sut = new DurationValidator();
        }


        [Fact]
        public void ReturnInvalidResult_GivenIncorrectEndDate()
        {
            var duration = new Duration(DateTime.Now, DateTime.Now.AddDays(-1d));
            var result = _sut.Validate(duration);

            result.IsValid.Should().BeFalse();
            result.Errors.Should().NotBeEmpty();
        }

        [Fact]
        public void ReturnValid_GivenNoEndDate()
        {
            var duration = new Duration(DateTime.Now, null);
            var result = _sut.Validate(duration);

            result.IsValid.Should().BeTrue();
            result.Errors.Should().BeEmpty();
        }
    }
}
