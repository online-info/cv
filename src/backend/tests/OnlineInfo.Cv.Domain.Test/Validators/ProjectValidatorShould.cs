﻿using FluentAssertions;
using OnlineInfo.Cv.Domain.Validators;
using System;
using Xunit;

namespace OnlineInfo.Cv.Domain.Test.Validators
{
    public class ProjectValidatorShould
    {
        private readonly ProjectValidator _sut;

        public ProjectValidatorShould()
        {
            _sut = new ProjectValidator();
        }

        [Fact]
        public void ReturnInvalidResult_GivenMissingTitle()
        {
            Project invalidProject = new Project
            {
                Title = "",
                Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(1d)),
                Description = ""
            };

            var validationResult = _sut.Validate(invalidProject);

            validationResult.IsValid.Should().BeFalse();
        }

        [Fact]
        public void ReturnInvalidResult_GivenExceededStringLength()
        {
            Project invalidProject = new Project
            {
                Title = new string('a', 101),
                Duration = new Duration(DateTime.Now, DateTime.Now.AddDays(1d)),
                Description = new string('a', 501)
            };

            var validationResult = _sut.Validate(invalidProject);

            validationResult.IsValid.Should().BeFalse();
            validationResult.Errors.Count.Should().Be(2);
        }

        [Fact]
        public void ValidateComplexProperties()
        {
            Duration invalidDuration = new Duration(DateTime.Now, DateTime.Now.AddDays(-1d));

            Project invalidProject = new Project
            {
                Title = "My Project",
                Duration = invalidDuration,
                Description = "My Description"
            };

            var validationResult = _sut.Validate(invalidProject);

            validationResult.IsValid.Should().BeFalse();
            validationResult.Errors.Count.Should().Be(1);
        }

        [Fact]
        public void GivenInvalidResult_GivenMissingDuration()
        {
            Project invalidProject = new Project
            {
                Title = "My Project",
                Duration = null,
                Description = "My Description"
            };

            var validationResult = _sut.Validate(invalidProject);

            validationResult.IsValid.Should().BeFalse();
            validationResult.Errors.Count.Should().Be(1);
        }

    }
}
