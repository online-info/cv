#!/bin/bash
# IMPORTANT! Make sure this file is saved with 'LF' file endings, otherwise it will not work
set -e
mongo <<- EOF
use admin
db.createUser({
user:  '$MONGO_INITDB_CV_USERNAME',
pwd: '$MONGO_INITDB_CV_PASSWORD',
roles: [{
role: 'readWrite',
db: '$MONGO_INITDB_CV_DATABASE'
}]
})
EOF
