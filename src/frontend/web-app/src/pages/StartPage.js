import React from 'react';
import { Article } from '../organisms/Article';


const StartPage = () => (
  <div id="articles" className="container-fluid">
    <Article title='What is this?' text="This website was made as a project by four students at IT-Högskolan in Gothenburg, Sweden. The purpouse of this project was to
          further our knowledge in CI/CD methodology and to gain skills in using Docker, GitLab CI/CD and various other technologies.
          We're all backend developers, so the styling of this page might be a bit off. ;)"
          imgSrc='../media/more_generic_tech.jpg'/>
    <Article title='Filler' text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec nunc ac magna rutrum tempor. Praesent id justo enim. Suspendisse vestibulum sodales nunc sit amet egestas. In posuere, urna a viverra elementum, sapien nisl tristique felis, id pellentesque lorem tortor ut eros. Quisque molestie lacus at euismod faucibus. Curabitur faucibus sollicitudin mi. Sed commodo ac eros vitae maximus. Aenean aliquet aliquet arcu, ut consequat ex pulvinar id. Sed neque urna, commodo a sapien eu, consectetur porta magna. Donec mattis aliquam ligula vel viverra. Integer vitae consectetur augue, et ornare enim. Quisque eget maximus risus.' 
            imgSrc='../media/other_tech_image.jpg'/>
    <div className="row">
      <div id="gitlab" className="col-12">
        <a href="https://gitlab.com/online-info/cv" className="text-decoration-none">
          <img src="/media/gitlab-icon-rgb.svg" className="mx-auto d-block" height="100px" width="100px" alt="GitLab Logo"/>
          <p className="text-light text-center">Check us out on GitLab!</p>
        </a>
      </div>
    </div>
  </div>
);

  export default StartPage;