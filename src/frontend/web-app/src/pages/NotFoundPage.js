import React from 'react';

const NotFoundPage = () => (
    <div className="text-center">
        <h1>404</h1>
        <h2>Ops, page not found!</h2>
        <h4>Check url!</h4>
        <a href="/" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Take me back</a>
    </div>
);

export default NotFoundPage;