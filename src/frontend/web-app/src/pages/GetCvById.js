import React, { Component } from 'react';
import CvDocument from '../templates/CvDocument';
import axios from 'axios';
import { PDFExport } from '@progress/kendo-react-pdf';
import moment from 'moment';
import './GetCvById.css';


export default class GetCvById extends Component {

    pdfExportComponent;

    constructor(props) {
        super(props);
    
        this.state = {
            isLoading: false, 
            cvDocument: {},
            print: false,
            stylePath: {},
            css: '',
          }
        };

    componentWillMount() {
        this.setState({ isLoading: true });
        axios.get(`client-api/api/Cv/${this.props.match.params.id}`)
          .then(result => {
            axios.get(`css/${result.data.stylePath}.css`).then(result => this.setState({css: result.data}));
            this.setState({ cvDocument: result.data, isLoading: false});
            this.props.callback({showButton: true, save: () => {
                this.setState({print: true},() => {
                    this.pdfExportComponent.save()
                    this.setState({print: false})
                });
            }});
            //console.log(result.data.summary);
          }).catch((ex) => {
           console.log(ex)
            if (ex.response.status === 404) {
              window.location.href = '/404';
            } 
          });
      };
 
    render() {
        let cv = this.state.cvDocument;

        return (
            this.state.isLoading ? <span className="text-muted">Loading...</span> :(
            <div className='container'>
             { this.state.stylePath && <style>{this.state.css}</style> } 

                {this.state.print ? (
                <PDFExport paperSize={'A4'}
                fileName={cv.info && cv.info.name ? cv.info.name.replace(" ","") + '_Resume_' + moment().format('YYMMDD') : 'noname'}
                title=""
                subject=""
                keywords=""
                keepTogether=""
                ref={(r) => this.pdfExportComponent = r}>
                    
                    <div className="print" style={{ /* Export container */
                        height: 842, 
                        width: 595,
                        padding: 'none',
                        backgroundColor: 'white',
                        margin: 'auto',
                        overflowX: 'hidden',
                        overflowY: 'hidden'}}>
                            <CvDocument cvDocument={cv}/>
                    </div>
                </PDFExport>) : 
                    (                       
                        /* Normal page display */
                        <CvDocument cvDocument={cv}/> 
                    )}
            </div> 
        ))
    }
}