import React, { Component } from 'react';
import SkillSection from '../organisms/SkillSection';
import CertificateSection from '../organisms/CertificateSection';
import EducationSection from '../organisms/EducationSection';
import ProjectSection from '../organisms/ProjectSection';
import ExperienceSection from '../organisms/ExperienceSection';
import UserInformationSection from '../organisms/UserInformationSection';

export default class CvDocument extends Component {

    constructor(props){
        super(props);

        this.state = {
            cvDocument: this.props.cvDocument,
            stylePath: this.props.cvDocument.stylePath
        };
    }

    render(){
        let cv = this.state.cvDocument;
        return(
            <div className="cv-wrapper">
                {cv.info && <UserInformationSection userInformation={cv.info} summary={cv.summary}/>}
                {cv.experience && cv.experience.length > 0 && <ExperienceSection experience={cv.experience}/>}
                {cv.projects && cv.projects.length > 0 && <ProjectSection projects={cv.projects}/>}
                <div className="cv-bottom">
                    {cv.education && cv.education.length > 0  && <EducationSection educations={cv.education}/>}
                    {cv.skills && cv.skills.length > 0 && <SkillSection skills={cv.skills}/>}
                    {cv.certificates && cv.certificates.length > 0 && <CertificateSection certificates={cv.certificates}/>}
                </div>
            </div>
        );
    }
}