import React from 'react';

export const SectionTitle = ({title}) => {
    return <h2 className="section-title">{title}</h2>
};