import React from 'react';
import moment from 'moment';    

export const Duration = ({duration}) => {
    return (
        <div className="duration">
            <span className='duration-start'>{moment(duration.startDate).format('MMMM YYYY')}</span><span> - </span><span className='duration-end'>{moment(duration.endDate).format('MMMM YYYY')}</span>
        </div>
    );
};