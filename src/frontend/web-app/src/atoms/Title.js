import React from 'react';

export const Title = ({title}) => {
    return <p className="title">{title}</p>
};