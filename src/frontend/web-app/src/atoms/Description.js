import React from 'react';

export const Description = ({description}) => {
    return <p className="description">{description}</p>
};