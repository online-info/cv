import React from 'react';

export const ArticleImage = ({imgSrc}) => {
    return (
        <div className="col-md-5 col-sm-12 article-image" style={{backgroundImage: `url(${imgSrc})`}} />
    );
}