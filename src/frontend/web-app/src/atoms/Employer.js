import React from 'react';

export const Employer = ({employer}) => {
    return (
        <p className="employer">{employer}</p>
    );
};