import React from 'react';

export const JobTitle = ({title}) => {
    return <p className="job-title">{title}</p>
};