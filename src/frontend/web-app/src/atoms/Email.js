import React from 'react';

export const Email = ({email}) => {
    return (<div className="email">
            <p className="key">Email</p>
            <p className="value">{email}</p>
        </div>)
};