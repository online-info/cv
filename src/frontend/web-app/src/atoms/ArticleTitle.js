import React from 'react';

export const ArticleTitle = ({title}) => {
    return <h3>{title}</h3>;
}