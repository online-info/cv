import React from 'react';

export const Summary = ({summary}) => {
    return <p className="summary">{summary}</p>
};