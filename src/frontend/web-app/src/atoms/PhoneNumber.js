import React from 'react';

export const PhoneNumber = ({number}) => {
    return (<div className="phone-number">
        <p className="key">Phone</p>
        <p className="value">{number}</p>
        </div>)
};