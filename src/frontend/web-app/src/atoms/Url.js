import React from 'react';

export const Url = ({url}) => {
    return <a className="url" href={url}>{url}</a>
};