import React from 'react';

export const Name = ({name}) => {
    return <p className="name">{name}</p>
};