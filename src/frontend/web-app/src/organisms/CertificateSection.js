import React from 'react';
import Certificate from '../molecules/Certificate';
import { SectionTitle } from '../atoms';

export default class CertificateSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {certificates: props.certificates}
    };
    render() {
        let mappedCertificates = this.state.certificates.map((c, i) => 
        <Certificate 
            key = {i}
            title = {c.title}
            url = {c.url}
        />
        );
        return (
            <div className="certificate-section">
                <SectionTitle title="Certificates"/>
                {mappedCertificates}
            </div>
        );
    };
};