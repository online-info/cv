import React from 'react';
import UserInformation from '../molecules/UserInformation';

export default class UserInformationSection extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            userInformation: props.userInformation,
            summary: props.summary
        };
    };
    render() {
        const user = this.state.userInformation;
        const summary = this.state.summary;
        return (
            <div className="user-information-section">
            <UserInformation 
                name = {user.name}
                summary = {summary}
                email = {user.email}
                phone = {user.phone}
                urls = {user.urls}
            /> 
            </div>
        );
    };
};