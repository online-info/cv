import React from 'react';
import Experience from '../molecules/Experience';
import { SectionTitle } from '../atoms';

export default class ExperienceSection extends React.Component {

    constructor(props){
        super(props);
        this.state = {experiences: props.experience}
    };
  
    render(){
        let mappedExperiences = this.state.experiences.map((e,i) => 
            <Experience 
                key={i}
                employer={e.employer}
                title={e.title}
                description={e.description}
                duration={e.duration}/>
        );

        return (
            <div className="experience-section">
                <SectionTitle title="Experience"/>
                {mappedExperiences}
            </div>
        );
    };
}