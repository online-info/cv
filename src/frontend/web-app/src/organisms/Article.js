import React from 'react';
import { ArticleImage } from '../atoms';
import { ArticleContent } from '../molecules/ArticleContent';

export const Article = ({title, text, imgSrc}) => {
    return (
        <div className="row article-row">
            <ArticleImage imgSrc={imgSrc}/>
            <ArticleContent title={title} text={text}/>
        </div>
    );
}