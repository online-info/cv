import React from 'react';
import Project from '../molecules/Project';
import { SectionTitle } from '../atoms';

export default class ProjectSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {projects: props.projects}
    };
    render() {
        let mappedProjects = this.state.projects.map((p, i) => 
            <Project 
                key = {i}
                title = {p.title}
                description = {p.description}
                duration = {p.duration}
            />
        );
        return (
            <div className="project-section">
                <SectionTitle title="Projects"/>
                {mappedProjects}
            </div>
        );
    };
};