import React from 'react';
import Skill from '../molecules/Skill';
import {SectionTitle} from '../atoms';

const SkillSection = (props) => {
    let mappedSkills = props.skills && props.skills.map((s,i) =>
    <Skill 
        key = {i}
        title = {s.title}
    />
    );
    return (
        <div className="skill-section">
            <SectionTitle title="Skills"/>
            {mappedSkills}
        </div>
    );
}

export default SkillSection;