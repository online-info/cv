import React from 'react';
import Education from '../molecules/Education';
import { SectionTitle } from '../atoms';

export default class EducationSection extends React.Component {
    
    constructor(props){
        super(props)

        this.state = {educations: props.educations};
    };
    
    render() {

        let mappedEducations = this.state.educations.map((e,i) => 
            <Education
            key = {i} 
            title = {e.title} 
            duration = {e.duration} />
        );

        return (
            <div className="education-section">
                <SectionTitle title="Education"/>
                {mappedEducations}
            </div>
        );
    };
};