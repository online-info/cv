import React from 'react';
import { ArticleTitle, ArticleText } from '../atoms';

export const ArticleContent = ({title, text}) => {
    return (
        <div className="col-md-6 col-sm-12 p-3 bg-white">
            <ArticleTitle title={title}/>
            <ArticleText text={text}/>
        </div>
    );
}