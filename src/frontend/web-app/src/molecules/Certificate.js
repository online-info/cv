import React from 'react';
import { Title, Url } from '../atoms';

const Certificate = ({title, url}) => {
    return (
        <div className="certificate no-split">
            <Title title = {title}/>
            <Url url = {url}/>
        </div>
    );
}

export default Certificate;