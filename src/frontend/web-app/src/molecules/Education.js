import React from 'react';
import { Title, Duration, Description } from '../atoms';

const Education = ({title, school, duration}) => {
    return (
        <div className="education no-split">
            <Title title = {school}/>
            <Description description = {title}/>
            <Duration duration = {duration}/>
        </div>
    );
}

export default Education;