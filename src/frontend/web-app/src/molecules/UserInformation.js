import React from 'react';
import { Name, Email, PhoneNumber, Summary, SectionTitle } from '../atoms';
import UserInformationUrls from './UserInformationUrls';

const UserInformation = ({name, email, phone, urls, summary}) => {
    return (
        <div  className="user-information no-split">
            <SectionTitle title="Info"/>
            <div className="user-information-title">
            <Name name = {name}/>
            <Summary summary = {summary}/>
            </div>
            <div className="info">
                <Email email = {email}/>
                <PhoneNumber number = {phone}/>
                <UserInformationUrls urls = {urls}/>
            </div>
        </div>
    );
};

export default UserInformation;