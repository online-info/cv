import React from 'react';
import { Description } from '../atoms';

const Skill = (skill) => {
    return (
        <div  className="skill no-split">
            <Description description={skill.title} />
        </div>
    );
};

export default Skill;