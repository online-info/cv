import React from 'react';
import { Url } from '../atoms';

const UserInformationUrls = (props) => {
    return (
    <div className="user-information-urls no-split">
    <p className="key">{props.urls.length > 1 ? "Sites" : "Site"}</p>
    <div className="value">
        {props.urls && props.urls.map((urlName, i) => 
            (<Url key={i} url={urlName}/>))}

    </div>
    </div>
    );
}; 

export default UserInformationUrls;
