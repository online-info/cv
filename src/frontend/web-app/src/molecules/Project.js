import React from 'react';
import {Title, Description, Duration } from '../atoms';

const Project = ({title, description, duration}) => {
    return (
        <div className="project no-split">
            <div className="devider">
                <Title title = {title} />
                <Duration duration = {duration} />
            </div>
            <Description description = {description} />
        </div>
    );
};

export default Project;