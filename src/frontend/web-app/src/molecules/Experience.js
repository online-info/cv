import React from 'react';
import { Duration, Description, Employer, JobTitle} from '../atoms';

const Experience = ({employer, description, duration, title}) => {
    return (
        <div className="experience no-split">
            <div className="devider">
                <Employer employer={employer}/>
                <JobTitle title={title}/>
                <Duration duration={duration}/>
            </div>
            <Description description={description}/>
        </div>
    );
};

export default Experience;