import React, { Component } from 'react';
import './App.css';
import {Route, BrowserRouter, Switch} from 'react-router-dom';
import StartPage from './pages/StartPage';
import GetCvById from './pages/GetCvById';
import NotFoundPage from './pages/NotFoundPage';



class App extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
        showButton: false,
        save: ''
      }
    };

  exportPDFWithComponent = () => {
    this.state.save();
}



  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <nav className="navbar navbar-dark bg-dark">
            <a className="navbar-brand text-light" href="/">

              <img src="#" height="30" width="30" alt="" />
              CV
            </a>
            {this.state.showButton ? <button onClick={this.exportPDFWithComponent} className="btn btn-light">Download as PDF</button> : <div/>}
          </nav>
          
          <Switch>
            <Route path="/" component={StartPage} exact={true} />
            <Route path="/404" component={NotFoundPage} exact={true} />
            <Route path="/:id" render={(props) => <GetCvById {... props} callback={x =>  this.setState(x)}/>} />
            <Route component={NotFoundPage} />
          </Switch>

          <div id="footer" className="bg-dark text-center text-muted">
            <span className="align-middle">&copy; {new Date().getFullYear()} Cv4Life</span>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
