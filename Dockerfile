
# stage: 2 — the production environment
FROM nginx:alpine
EXPOSE 3000
COPY src/frontend/web-app/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=frontend-build /app /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]

