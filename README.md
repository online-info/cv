# cv


## Built with

- [ASP.NET Core]()
- [Serilog](https://serilog.net/)
- [Fluent Assertions](https://fluentassertions.com)
- [FakeItEasy](https://github.com/FakeItEasy/FakeItEasy)

## Useful commands

```bash
# Delete all containers
docker rm $(docker ps -a -q)
# Delete all images
docker rmi $(docker images -q)
# Delete all unused files
docker image prune --all
# mount src folder
sudo mount -t vboxsf src /home/erik/src/
# rebuild and recreate all containers
sudo docker-compose -f docker-compose-dev.yml up -d --force-recreate --build
```
